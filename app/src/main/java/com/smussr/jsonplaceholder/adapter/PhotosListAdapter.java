package com.smussr.jsonplaceholder.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.smussr.jsonplaceholder.R;
import com.smussr.jsonplaceholder.model.ModelPhotos;

import java.util.List;

public class PhotosListAdapter extends RecyclerView.Adapter<PhotosListAdapter.MyViewHolder> {

    private List<ModelPhotos> photoData;
    private final Context context;

    public PhotosListAdapter(List<ModelPhotos> albumData, Context context) {
        this.context = context;
        this.photoData = albumData;
    }

    @Override
    public PhotosListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.photos_items, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        final ModelPhotos photoItem = photoData.get(i);
        myViewHolder.photo_name.setText(photoItem.getTitle());
        Glide.with(context).load(photoItem.getThumbnailUrl()).format(DecodeFormat.PREFER_ARGB_8888).placeholder(R.drawable.holder_img).into(myViewHolder.photo);

    }

    @Override
    public int getItemCount() {
        return photoData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView photo_name;
        public ImageView photo;

        public MyViewHolder(View v) {
            super(v);
           photo_name = (TextView) v.findViewById(R.id.photo_name);
            photo = (ImageView) v.findViewById(R.id.photo_item);

        }
    }
}
