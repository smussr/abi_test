package com.smussr.jsonplaceholder.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smussr.jsonplaceholder.activity.Photos_Act;
import com.smussr.jsonplaceholder.R;
import com.smussr.jsonplaceholder.model.ModelAlbum;

import java.util.List;

import static com.smussr.jsonplaceholder.WebConstant.A_ID;

public class AlbumListAdapter extends RecyclerView.Adapter<AlbumListAdapter.MyViewHolder> {

    private List<ModelAlbum> albumData;
    private final Context context;

    public AlbumListAdapter(List<ModelAlbum> albumData, Context context) {
        this.context = context;
        this.albumData = albumData;
    }

    @Override
    public AlbumListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.album_items, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        final ModelAlbum albumItem = albumData.get(i);
        myViewHolder.album_name.setText(albumItem.getTitle().trim());

        myViewHolder.LLY_lay_class.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent s = new Intent(context, Photos_Act.class);
                s.putExtra(A_ID, albumItem.getId());
                s.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(s);
            }
        });
    }

    @Override
    public int getItemCount() {
        return albumData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView album_name;
        public ImageView album_cover;
        LinearLayout LLY_lay_class;

        public MyViewHolder(View v) {
            super(v);
            album_name = (TextView) v.findViewById(R.id.album_name);
            LLY_lay_class = (LinearLayout) v.findViewById(R.id.lay_class_);

        }
    }
}
