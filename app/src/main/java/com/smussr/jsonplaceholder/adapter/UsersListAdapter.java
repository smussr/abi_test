package com.smussr.jsonplaceholder.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.smussr.jsonplaceholder.model.ModelUser;
import com.smussr.jsonplaceholder.activity.Profile_Act;
import com.smussr.jsonplaceholder.R;

import java.util.List;

import static com.smussr.jsonplaceholder.WebConstant.U_ADDRESS;
import static com.smussr.jsonplaceholder.WebConstant.U_EMAIL;
import static com.smussr.jsonplaceholder.WebConstant.U_ID;
import static com.smussr.jsonplaceholder.WebConstant.U_NAME;

public class UsersListAdapter extends RecyclerView.Adapter<UsersListAdapter.MyViewHolder> {

    private List<ModelUser> listData;
    private final Context context;

    public UsersListAdapter(List<ModelUser> listData, Context context) {
        this.context = context;
        this.listData = listData;
    }

    @Override
    public UsersListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_items, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        final ModelUser modelUser = listData.get(i);
        myViewHolder.user_name.setText(modelUser.getName());
        myViewHolder.user_email.setText(modelUser.getEmail());
        Glide.with(context).load(modelUser.getName()).format(DecodeFormat.PREFER_ARGB_8888).placeholder(R.drawable.holder_img).into(myViewHolder.item_image);

        myViewHolder.LLY_lay_class.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent s = new Intent(context, Profile_Act.class);
                s.putExtra(U_ID, modelUser.getId());
                s.putExtra(U_NAME, modelUser.getName());
                s.putExtra(U_EMAIL, modelUser.getEmail());
                s.putExtra(U_ADDRESS, modelUser.getAddress());
                s.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(s);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView user_name, user_email;
        public ImageView item_image;
        LinearLayout LLY_lay_class;

        public MyViewHolder(View v) {
            super(v);
            user_name = (TextView) v.findViewById(R.id.user_name);
            user_email = (TextView) v.findViewById(R.id.user_email);
            item_image = (ImageView) v.findViewById(R.id.item_image);
            LLY_lay_class = (LinearLayout) v.findViewById(R.id.lay_class);

        }
    }
}
