package com.smussr.jsonplaceholder;


public class WebConstant {

    private static String BASE_URL;

    static {
        BASE_URL = BuildConfig.BASE_URL;
    }

    public static final String BASE = BASE_URL;
    //=========================================LINKS==============================================//
    public static final String GET_USERS = BASE_URL + "/users";
    public static final String GET_ALBUMS = BASE_URL + "/albums";
    public static final String GET_PHOTOS = BASE_URL + "/photos";
    //=========================================FIELDS==============================================//
    public static final String U_ID = "id";
    public static final String U_NAME = "name";
    public static final String U_USER_NAME = "username";
    public static final String U_EMAIL = "email";
    public static final String U_ADDRESS = "address";

    public static final String A_ID = "id";
    public static final String A_U_ID = "userId";
    public static final String A_TITLE = "title";

    public static final String P_ID = "id";
    public static final String P_A_ID = "albumId";
    public static final String P_TITLE = "title";
    public static final String P_URL = "url";
    public static final String P_THUMB = "thumbnailUrl";

}