package com.smussr.jsonplaceholder;

import android.app.ProgressDialog;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.smussr.jsonplaceholder.adapter.UsersListAdapter;
import com.smussr.jsonplaceholder.model.ModelUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.smussr.jsonplaceholder.WebConstant.GET_USERS;
import static com.smussr.jsonplaceholder.WebConstant.U_ADDRESS;
import static com.smussr.jsonplaceholder.WebConstant.U_EMAIL;
import static com.smussr.jsonplaceholder.WebConstant.U_ID;
import static com.smussr.jsonplaceholder.WebConstant.U_NAME;
import static com.smussr.jsonplaceholder.WebConstant.U_USER_NAME;

public class MainActivity extends AppCompatActivity {

    private TextView txtEmpty;
    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeContainer;

    private RecyclerView listView;
    protected static List<ModelUser> listData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        txtEmpty = (TextView) findViewById(R.id.empty);

        listView = (RecyclerView) findViewById(R.id.list_view);
        listView.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        listView.setLayoutManager(llm);


        swipeContainer.setColorSchemeColors(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorAccent);
        swipeContainer.setDistanceToTriggerSync(15);
        swipeContainer.setSize(SwipeRefreshLayout.DEFAULT);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
            }
        });

        loadData();

        startProcessing();
    }

    public void loadData() {
        listData = new ArrayList<>();
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching Data");
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, GET_USERS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                endProcessing();
                try {
                    Log.d("RESPONSE : ", response);
                    JSONArray jsonArray = new JSONArray(response);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String pkg = jsonObject.getString(U_ADDRESS);
                        ModelUser item = new ModelUser(
                                jsonObject.getString(U_ID),
                                jsonObject.getString(U_NAME),
                                jsonObject.getString(U_USER_NAME),
                                jsonObject.getString(U_EMAIL),
                                pkg
                        );
                        listData.add(item);
                    }
                    UsersListAdapter adapter = new UsersListAdapter(listData, getApplicationContext());
                    listView.setAdapter(adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                setHistoryEmptyView();
                endProcessing();
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }



    private void startProcessing() {
        txtEmpty.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void endProcessing() {
        swipeContainer.setRefreshing(false);
        txtEmpty.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
    }

    private void setHistoryEmptyView() {
        progressBar.setVisibility(View.GONE);
        txtEmpty.setText("No Data Found..");
        txtEmpty.setVisibility(View.VISIBLE);
    }

}
