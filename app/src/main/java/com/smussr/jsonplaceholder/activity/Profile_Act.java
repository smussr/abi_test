package com.smussr.jsonplaceholder.activity;

import android.app.ProgressDialog;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.smussr.jsonplaceholder.R;
import com.smussr.jsonplaceholder.adapter.AlbumListAdapter;
import com.smussr.jsonplaceholder.model.ModelAlbum;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.smussr.jsonplaceholder.WebConstant.A_ID;
import static com.smussr.jsonplaceholder.WebConstant.A_TITLE;
import static com.smussr.jsonplaceholder.WebConstant.A_U_ID;
import static com.smussr.jsonplaceholder.WebConstant.GET_ALBUMS;
import static com.smussr.jsonplaceholder.WebConstant.U_EMAIL;
import static com.smussr.jsonplaceholder.WebConstant.U_ID;
import static com.smussr.jsonplaceholder.WebConstant.U_NAME;


public class Profile_Act extends AppCompatActivity {

    private TextView txtEmpty;
    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeContainer;

    public AppBarLayout appBarLayout;
    public LinearLayout lay_background;
    public View headerView, headerViewBlue;


    ImageView imageView;
    TextView user_name, user_email;
    RecyclerView recyclerView;
    JSONArray album_array = null;
    protected static List<ModelAlbum> album_listData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_);

        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        txtEmpty = (TextView) findViewById(R.id.empty);

        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        lay_background = (LinearLayout) findViewById(R.id.lay_background);
        headerView = (View) findViewById(R.id.header_view);
        headerViewBlue = (View) findViewById(R.id.header_view_blue);

        imageView = (ImageView) findViewById(R.id.imageView);
        user_name = (TextView) findViewById(R.id.textView);
        user_name.setText(getIntent().getStringExtra(U_NAME));
        user_email = (TextView) findViewById(R.id.email);
        user_email.setText(getIntent().getStringExtra(U_EMAIL));
        Glide.with(this).load(getIntent().getStringExtra(U_NAME)).format(DecodeFormat.PREFER_ARGB_8888).placeholder(R.drawable.holder_img).into(imageView);

        recyclerView = (RecyclerView) findViewById(R.id.album_list_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);//defines recycler view look
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);


        swipeContainer.setColorSchemeColors(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorAccent);
        swipeContainer.setDistanceToTriggerSync(15);
        swipeContainer.setSize(SwipeRefreshLayout.DEFAULT);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
            }
        });

        loadData();
        startProcessing();


        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                float percentage = 1 - ((float) Math.abs(verticalOffset) / appBarLayout.getTotalScrollRange());
                float perce = (float) Math.abs(verticalOffset) / appBarLayout.getTotalScrollRange();
                lay_background.setAlpha(percentage);
                headerViewBlue.setAlpha(percentage);

                if (perce < 0.5) {
                    headerView.setVisibility(View.VISIBLE);
                }
                headerView.setAlpha(perce);

            }
        });
    }

    private void loadData() {
        album_listData = new ArrayList<>();
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching Data");
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, GET_ALBUMS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                endProcessing();
                try {
                    Log.d("RESPONSE : ", response);
                    JSONArray jsonArray = new JSONArray(response);
                    String UserID = getIntent().getStringExtra(U_ID);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        if (jsonObject.getString(A_U_ID).equals(UserID)) {
                            ModelAlbum item = new ModelAlbum(
                                    jsonObject.getString(A_U_ID),
                                    jsonObject.getString(A_ID),
                                    jsonObject.getString(A_TITLE)
                            );
                            album_listData.add(item);
                        }
                    }
                    AlbumListAdapter adapter = new AlbumListAdapter(album_listData, getApplicationContext());
                    recyclerView.setAdapter(adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                setHistoryEmptyView();
                endProcessing();
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    private void startProcessing() {
        txtEmpty.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void endProcessing() {
        swipeContainer.setRefreshing(false);
        txtEmpty.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
    }

    private void setHistoryEmptyView() {
        progressBar.setVisibility(View.GONE);
        txtEmpty.setText("No Data Found..");
        txtEmpty.setVisibility(View.VISIBLE);
    }
}