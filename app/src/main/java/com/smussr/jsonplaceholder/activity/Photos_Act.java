package com.smussr.jsonplaceholder.activity;

import android.app.ProgressDialog;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.smussr.jsonplaceholder.R;
import com.smussr.jsonplaceholder.adapter.AlbumListAdapter;
import com.smussr.jsonplaceholder.adapter.PhotosListAdapter;
import com.smussr.jsonplaceholder.model.ModelAlbum;
import com.smussr.jsonplaceholder.model.ModelPhotos;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.smussr.jsonplaceholder.WebConstant.A_ID;
import static com.smussr.jsonplaceholder.WebConstant.GET_PHOTOS;
import static com.smussr.jsonplaceholder.WebConstant.P_A_ID;
import static com.smussr.jsonplaceholder.WebConstant.P_ID;
import static com.smussr.jsonplaceholder.WebConstant.P_THUMB;
import static com.smussr.jsonplaceholder.WebConstant.P_TITLE;
import static com.smussr.jsonplaceholder.WebConstant.P_URL;

public class Photos_Act extends AppCompatActivity {

    private TextView txtEmpty;
    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeContainer;

    private RecyclerView recyclerView;
    protected static List<ModelPhotos> photosData;
    JSONArray photos_array = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos_);

        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        txtEmpty = (TextView) findViewById(R.id.empty);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(this);//defines recycler view look
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);

        swipeContainer.setColorSchemeColors(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorAccent);
        swipeContainer.setDistanceToTriggerSync(15);
        swipeContainer.setSize(SwipeRefreshLayout.DEFAULT);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
            }
        });

        loadData();
        startProcessing();
    }

    private void loadData() {
        photosData = new ArrayList<>();
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching Data");
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, GET_PHOTOS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                endProcessing();
                try {
                    Log.d("RESPONSE : ", response);
                    JSONArray jsonArray = new JSONArray(response);
                    String AlbumID = getIntent().getStringExtra(A_ID);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        if (jsonObject.getString(P_A_ID).equals(AlbumID)) {
                            ModelPhotos item = new ModelPhotos(
                                    jsonObject.getString(P_A_ID),
                                    jsonObject.getString(P_ID),
                                    jsonObject.getString(P_TITLE),
                                    jsonObject.getString(P_URL),
                                    jsonObject.getString(P_THUMB)
                            );
                            photosData.add(item);
                        }
                    }
                    PhotosListAdapter adapter = new PhotosListAdapter(photosData, getApplicationContext());
                    recyclerView.setAdapter(adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                setHistoryEmptyView();
                endProcessing();
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void startProcessing() {
        txtEmpty.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void endProcessing() {
        swipeContainer.setRefreshing(false);
        txtEmpty.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
    }

    private void setHistoryEmptyView() {
        progressBar.setVisibility(View.GONE);
        txtEmpty.setText("No Data Found..");
        txtEmpty.setVisibility(View.VISIBLE);
    }
}
